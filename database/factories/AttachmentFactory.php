<?php

namespace KDA\Laravel\Attachments\Database\Factories;

use KDA\Laravel\Attachments\Models\Attachment;
use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\Attachments\Models\File;
use KDA\Tests\Models\Post;

class AttachmentFactory extends Factory
{
    protected $model = Attachment::class;

    public function definition()
    {
        $model = Post::factory()->create();
        return [
            //
            'model_id'=>$model->getKey(),
            'model_type'=>get_class($model),
            'file_id'=> File::factory()
        ];

    }
}
