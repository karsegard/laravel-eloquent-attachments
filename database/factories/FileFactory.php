<?php

namespace KDA\Laravel\Attachments\Database\Factories;

use KDA\Laravel\Attachments\Models\File;
use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Tests\Models\Post;

class FileFactory extends Factory
{
    protected $model = File::class;

    public function definition()
    {
       
        return [
            //
            
            'filename'=>$this->faker->word().".".$this->faker->fileExtension(),
            'original_filename'=>$this->faker->word().".".$this->faker->fileExtension(),
            'extension'=>$this->faker->fileExtension(),
            'disk'=>'attachments',
            'mime_type'=>$this->faker->mimeType(),
            'size'=>$this->faker->numberBetween(0,50000),
            'path'=> $this->faker->filePath(),
        ];
    }
}
