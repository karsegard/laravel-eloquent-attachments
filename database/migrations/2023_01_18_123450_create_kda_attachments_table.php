<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('kda_at_files', function (Blueprint $table) {
            $table->id();
            $table->string('original_filename');
            $table->string('filename');
            $table->string('extension');
            $table->string('mime_type');
            $table->string('path');
            $table->string('disk'); // Storage disk
            $table->foreignId('user_id')->nullable();
            $table->unsignedBigInteger('size');
            $table->json('meta')->nullable();
            $table->timestamps();

        });

        Schema::create('kda_at_attachments', function (Blueprint $table) {
            $table->id();
            $table->numericMorphs('model');
            $table->foreignId('file_id');
            $table->json('meta')->nullable();
            $table->timestamps();

        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('kda_at_attachments');
        Schema::dropIfExists('kda_at_files');
     
        Schema::enableForeignKeyConstraints();
    }
};