<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Attachments\Models\Attachment;
use KDA\Laravel\Attachments\Models\File;
use KDA\Tests\Models\Post;
use KDA\Tests\Models\User;
use KDA\Tests\TestCase;

class ModelTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function can_create_file()
  {
    $o = File::factory()->create([]);
    $this->assertNotNull($o);
  }


  /** @test */
  function can_create_attachment()
  {
    $o = Attachment::factory()->create([]);
    $this->assertNotNull($o);
  }
  /** @test */


  function can_add_attachment()
  {
    $p = Post::factory()->create();

    $a = $p->addAttachment(public_path('docs/test.txt'));
    $this->assertNotNull($p);
    $this->assertNotNull($a);
    $this->assertNotNull($p->attachments);
    $this->assertEquals(1,$p->attachments->count());
    $this->assertNotNull($p->attachments->first()->file);
    
  }
  /** @test */

  function can_remove_attachments()
  {
    $p = Post::factory()->create();

    $a = $p->addAttachment(public_path('docs/test.txt'));
    $this->assertEquals(1,$p->attachments->count());

    $p->detachAttachemnt($a);
    $this->assertEquals(0,$p->attachments->count());
  }

 /** @test */


 function authenticatedUserAddsAttachment()
 {
   $p = Post::factory()->create();
   $user = User::factory()->create();
   $this->actingAs($user);
   $a = $p->addAttachment(public_path('docs/test.txt'));

   $this->assertNotNull($a->file->user_id);
   
 }

  
}