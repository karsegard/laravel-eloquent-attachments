<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Attachments\Models\Attachment;
use KDA\Laravel\Attachments\Models\File;
use KDA\Taggable\Models\Tag;
use KDA\Tests\Models\Post;
use KDA\Tests\TestCase;
use KDA\Taggable\Facades\Tags;

class IntegrationWithTagsTest extends TestCase
{
  use RefreshDatabase;

  /** @test */

  function can_add_tag_to_attachment()
  {
    $p = Post::factory()->create();

    $a = $p->addAttachment(public_path('docs/test.txt'));

    $t = Tag::factory()->create(['group'=>'files']);

    
    Tags::syncExistingTagsIdsWithType($a,[$t->id],'files');


    $this->assertEquals(1,$a->refresh()->tags->count());
  }

  /** @test */

  function can_clear_attachment_tags()
  {
    $p = Post::factory()->create();

    $a = $p->addAttachment(public_path('docs/test.txt'));
    $a2 = $p->addAttachment(public_path('docs/test.txt'));

    $t = Tag::factory()->create(['group'=>'files']);
    $t2= Tag::factory()->create(['group'=>'files']);

    
    Tags::syncExistingTagsIdsWithType($a,[$t->id,$t2->id],'files');
    Tags::syncExistingTagsIdsWithType($a2,[$t2->id],'files');


    $this->assertEquals(2,$a->refresh()->tags->count());
    $this->assertEquals(1,$a2->refresh()->tags->count());

    Tags::clearTags($a,$group='files');
    $this->assertEquals(0,$a->refresh()->tags->count());
    $this->assertEquals(1,$a2->refresh()->tags->count());
  }


  
}