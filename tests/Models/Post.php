<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Laravel\Attachments\Models\Traits\HasAttachments;

class Post extends Model 
{
   
    use HasFactory;
    use HasAttachments;
    protected $fillable = [
        'title'
    ];

    protected static function newFactory()
    {
        return  \KDA\Tests\Database\Factories\PostFactory::new();
    }

}
