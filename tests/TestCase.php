<?php

namespace KDA\Tests;

use KDA\Laravel\Attachments\ServiceProvider as ServiceProvider;
use Orchestra\Testbench\TestCase as ParentTestCase;
use Illuminate\Foundation\Testing\RefreshDatabaseState;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Support\Facades\Storage;
use KDA\Laravel\Attachments\Models\Attachment;
use KDA\Taggable\Facades\Tags;

use KDA\Taggable\ServiceProvider as TagServiceProvider;
class TestCase extends ParentTestCase
{

  public function setUp(): void
  {
    parent::setUp();
  }

  protected function getPackageProviders($app)
  {
    return [
      TagServiceProvider::class,
      ServiceProvider::class
    ];
  }

  

  protected function runPackageMigrations($app)
  {
    $provider = ServiceProvider::class;//$this->getPackageProviders($app)[0];
    $traits = class_uses_recursive($provider);

    if (in_array('KDA\Laravel\Traits\HasLoadableMigration', $traits)) {
     
      $this->artisan('migrate', ['--database' => 'mysql'])->run();
      $this->beforeApplicationDestroyed(function () {
        $this->artisan('migrate:rollback', ['--database' => 'mysql'])->run();
        RefreshDatabaseState::$migrated = false;

      });
    }
  }
  protected function callAfterResolving($name, $callback)
  {
      $this->app->afterResolving($name, $callback);

      if ($this->app->resolved($name)) {
          $callback($this->app->make($name), $this->app);
      }
  }

  protected function loadTestMigrations($paths)
  {
      $this->callAfterResolving('migrator', function ($migrator) use ($paths) {
          foreach ((array) $paths as $path) {
              $migrator->path($path);
          }
      });
  }
  protected function tearDown(): void
  {
      $dirs = Storage::disk('attachments')->files();
      foreach ($dirs as $dir) {
          Storage::disk('attachments')->delete($dir);
      }
      parent::tearDown();
  }

  protected function getEnvironmentSetUp($app)
  {
    $this->app = $app;

    $this->app->instance('path.public', __DIR__.DIRECTORY_SEPARATOR.'public');
    Tags::registerTaggableModel(Attachment::class);

    $app['config']->set('filesystems.disks.attachments.driver', 'local');
    $app['config']->set('filesystems.disks.attachments.root', realpath(__DIR__.'/storage/attachments'));
    $app['config']->set('filesystems.disks.attachments.throw', false);
    $app['config']->set('filesystems.disks.attachments.visibility', 'private');

    $this->loadTestMigrations([__DIR__.'/database/migrations']);
    $this->runPackageMigrations($app);
   
  }
}
