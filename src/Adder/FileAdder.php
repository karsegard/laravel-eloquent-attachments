<?php
namespace KDA\Laravel\Attachments\Adder;


use Illuminate\Http\File;
use LiveWire\TemporaryUploadedFile;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Traits\Conditionable;
use KDA\Laravel\Attachments\Adder\Concerns\EvaluatesClosure;
use KDA\Laravel\Attachments\Adder\Concerns\HasDisk;
use KDA\Laravel\Attachments\Adder\Concerns\HasFile;
use KDA\Laravel\Attachments\Adder\Concerns\HasModel;
use KDA\Laravel\Attachments\Adder\Concerns\HasPath;
use KDA\Laravel\Attachments\Adder\Concerns\HasVisibility;
use KDA\Laravel\Attachments\Adder\Concerns\HandleFileModel;
use KDA\Laravel\Attachments\Models\Attachment;
use KDA\Laravel\Attachments\Models\File as ModelsFile;

class FileAdder
{
    use EvaluatesClosure;
    use HasDisk;
    use HasFile;
    use HasModel;
    use HasPath;
    use HasVisibility;
    use HandleFileModel;
    use Conditionable;
    

    protected ?Attachment  $attachment = null;

    public function attachment(?Attachment $attachment):static
    {
        $this->attachment = $attachment;
        return $this;
    }
   
    public function getAttachment():?Attachment
    {
        return $this->attachment;
    }

    public function attachToModel(array | null $meta=null):static{
        $model = $this->getModel();
        $file = $this->getFileModel();
        if(!$model || !$file){
            throw new \Exception('you have to specify a model and a file to use this funciton');
        }

        $this->attachment(Attachment::create([
            'model_id'=>$model->getKey(),
            'model_type'=>get_class($model),
            'file_id'=>$file->getKey(),
            'meta'=>$meta
        ]));

        return $this;
    }

    public function detachFromModel(){
        $attachment = $this->getAttachment();

        $model = $this->getModel();
        if(!$model || $attachment->model_id==$model->getKey() && $attachment->model_type == get_class($model)){
            $attachment->delete();

        }
        $this->attachment(null);
        return $this;
    }

   
}
