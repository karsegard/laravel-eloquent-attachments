<?php

namespace KDA\Laravel\Attachments\Adder\Concerns;

use Closure;

trait HasPath{
    protected string | Closure | null $path;


    public function path(string | Closure | null $path):static
    {
        $this->path = $path;
        return $this;
    }


    public function getPath(){
        $path = $this->evaluate($this->path);
        if(blank($path)){
            $path =  config('kda.attachments.path', '');
        }
        return $path;
    }
}