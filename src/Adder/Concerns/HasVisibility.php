<?php

namespace KDA\Laravel\Attachments\Adder\Concerns;

use Closure;

trait HasVisibility{
    protected string | Closure $visibility = 'private';


    public function visibility(string | Closure $visibility):static
    {
        $this->visibility = $visibility;
        return $this;
    }


    public function getVisibility(){
        $visibility = $this->evaluate($this->visibility);
        if(blank($visibility)){
            $visibility =  config('kda.attachments.visibility', 'private');
        }
        return $visibility;
    }
}