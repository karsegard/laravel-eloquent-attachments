<?php

namespace KDA\Laravel\Attachments\Adder\Concerns;

use Closure;

trait HasDisk{
    protected string | Closure | null $disk;

    public function disk(string | Closure | null $disk):static
    {
        $this->disk = $disk;
        return $this;
    }


    public function getDisk(){
        $disk = $this->evaluate($this->disk);
        if(blank($disk)){
            $disk =  config('kda.attachments.disk', 'attachments');
        }
        return $disk;
    }
}