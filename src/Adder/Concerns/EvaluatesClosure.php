<?php

namespace KDA\Laravel\Attachments\Adder\Concerns;

use Closure;

trait EvaluatesClosure
{
    public function evaluate($value, array $parameters = [])
    {
        if ($value instanceof Closure) {
            return app()->call(
                $value,
                $parameters
            );
        }

        return $value;
    }
}
