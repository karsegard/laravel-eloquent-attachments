<?php

namespace KDA\Laravel\Attachments\Adder\Concerns;

use Closure;
use Illuminate\Support\Facades\Storage;
use KDA\Laravel\Attachments\Models\File;

trait HandleFileModel{
    protected File | Closure $file_model;


    public function fileModel(File | Closure $file_model):static
    {
        $this->file_model = $file_model;
        return $this;
    }


    public function getFileModel():?File{
        $file_model = $this->evaluate($this->file_model);
       
        return $file_model;
    }

    public function storeFile():static{
        $disk = $this->getDisk();
        $path = $this->getPath();
        $model = $this->getModel();
        $storage= Storage::disk($disk);

        $file = $this->getFile();
        $visibility = $this->getVisibility();
        $extension = $file->extension();
        $filename = (string) \Str::uuid().".".$extension;
        $storage->putFileAs($path, $file , $filename,$visibility);

        $attachment = [
          /*  'model_id'=>$model->getKey(),
            'model_type'=>get_class($model),*/
            'original_filename'=> $this->getOriginalFilename(),
            'extension'=>$extension,
            'size'=>$file->getSize(),
            'mime_type'=>$this->getMimeType(), 
            'user_id'=>auth()->user()?->getKey(),
            'filename'=>$filename,
            'path'=>$path,
            'disk'=>$disk
        ];

        $this->fileModel(File::create($attachment));
        
        

        return $this;
    }
}