<?php

namespace KDA\Laravel\Attachments\Adder\Concerns;

use Illuminate\Http\File;
use LiveWire\TemporaryUploadedFile;
use Illuminate\Http\UploadedFile;
trait HasFile{
    protected string | File | TemporaryUploadedFile |  UploadedFile  $file;

    


    public function file (string | File | TemporaryUploadedFile |  UploadedFile $file):static
    {
       $this->file = $file;
        return $this;
    }

    public function getFile(){
        
        $file =  $this->file;

        if(is_string($file)){
            $file = new File($file);
        }

        return $file;
    }

    public function getOriginalFilename():string
    {
        $file = $this->getFile();
        if($file instanceof File){
            return $file->getFilename();
        }
        return $file->getClientOriginalName();
    }

    public function getMimeType():string
    {
        $file = $this->getFile();
        if($file instanceof File){
            return $file->getMimeType();
        }
        return $file->getClientMimeType();
    }
}