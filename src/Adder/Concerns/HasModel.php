<?php

namespace KDA\Laravel\Attachments\Adder\Concerns;

use Illuminate\Database\Eloquent\Model;

trait HasModel{
    protected Model $model;
    public function model(Model $model):static
    {
        $this->model = $model;
        return $this;
    }
    public function getModel(){
        return $this->model;
    }
}