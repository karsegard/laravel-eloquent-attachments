<?php

namespace KDA\Laravel\Attachments\Facades;

use Illuminate\Support\Facades\Facade;

class AttachmentManager extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
