<?php

namespace KDA\Laravel\Attachments\Models\Traits;

use KDA\Laravel\Attachments\Models\Attachment;
use LiveWire\TemporaryUploadedFile;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\File;
use KDA\Laravel\Attachments\Adder\FileAdder;
use KDA\Laravel\Attachments\Facades\AttachmentManager;
use KDA\Laravel\Attachments\Models\File as ModelsFile;

trait HasAttachments
{

    public static function bootHasAttachments(): void
    {
        static::creating(function ($model)
        {
        });
        static::updating(function ($model)
        {
        });
    }


    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'model');
    }

    public function addAttachment(string | UploadedFile | File | ModelsFile $file,$meta=[],$path=null,$disk=null):Attachment
    {
        $adder=  AttachmentManager::addAttachmentToModel($this,$file,$meta,$path,$disk);
     //   $this->refresh();
        return $adder;
    }
    
    public function detachAttachemnt(Attachment  $file):FileAdder
    {
        $result =  AttachmentManager::detachAttachmentFromModel($this,$file);
   //     $this->refresh();
        return $result;
    }
}
