<?php

namespace KDA\Laravel\Attachments\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Attachment extends Model
{
    use HasFactory;

    public $table=  'kda_at_attachments';
    protected $fillable = [
      
        'model_type',
        'model_id',
        'file_id',
        'meta',
    ];

    protected $appends = [];

    protected $casts = [
        'meta'=>'json'
    ];


    protected static function newFactory()
    {
        return  \KDA\Laravel\Attachments\Database\Factories\AttachmentFactory::new();
    }

    public function file(){
        return $this->belongsTo(File::class,'file_id');
    }

}
