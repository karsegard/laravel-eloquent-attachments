<?php

namespace KDA\Laravel\Attachments\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    use HasFactory;

    public $table=  'kda_at_files';
    protected $fillable = [
        'filename',
        'original_filename',
        'user_id',
        'extension',
        'mime_type',
        'disk',
        'size',
        'path',
    ];

    protected $appends = [];

    protected $casts = [];


    protected static function newFactory()
    {
        return  \KDA\Laravel\Attachments\Database\Factories\FileFactory::new();
    }
    public function user() {
        return $this->belongsTo(User::class,'user_id');
    }

    public function download(){
        return Storage::disk($this->disk)->download($this->path.'/'.$this->filename,null,[
            'Content-Type'=>$this->mime_type
        ]);
    }

    public function getTemporaryUrl($expire){
        return Storage::disk($this->disk)->temporaryUrl($this->filename, $expire);
    }
}
