<?php
namespace KDA\Laravel\Attachments;

//use Illuminate\Support\Facades\Blade;

use Illuminate\Database\Eloquent\Model;
use LiveWire\TemporaryUploadedFile;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use KDA\Laravel\Concerns\CanIgnoreMigrations;
use KDA\Laravel\Attachments\Models\File as ModelsFile;
use KDA\Laravel\Attachments\Adder\FileAdder;
use KDA\Laravel\Attachments\Models\Attachment;

class AttachmentManager 
{
   use CanIgnoreMigrations;
/*
    public function getDisk(): string
    {
        return config('kda.laravel-attachments.disk', 'attachments');
    }

    public function getPath(): string
    {
        return config('kda.laravel-attachments.path', '');
    }

    public function getVisibility(): string
    {
        return config('kda.laravel-attachments.visibility', 'private');
    }

    public function storeAttachment(string | UploadedFile | File  | TemporaryUploadedFile $file, $path=null ,$disk = null): ?string
    {
        $disk = empty($disk) ? $this->getDisk() : $disk;
        $filename = (string) \Str::uuid();
        $path = $this->getPath();
        $storage = Storage::disk($disk);

        if (is_string($file)) {
            $file =new File($file) ;
        }

        $storage->putFileAs($path, $file , $filename,$this->getVisibility());
        return $filename;
    }

    public function fileInfo(File $file){
        return [
            'original_filename'=> $file->getBasename(),
            'extension'=>$file->getExtension(),
            'size'=>$file->getSize()
        ];
    }
*/
    public function addAttachmentToModel($model,string | UploadedFile | File | ModelsFile $file,$meta=[],$path=null,$disk=null){
        $fileAdder = app(FileAdder::class);
        return $fileAdder
        ->model($model)
        ->when(! ($file instanceof ModelsFile),fn ($adder)=>$adder->file($file)
            ->path($path)
            ->disk($disk)
            ->storeFile()
        )
        ->when($file instanceof ModelsFile, fn($adder)=> $adder->fileModel($file))
        ->attachToModel($meta)
        ->getAttachment();
    }

    public function detachAttachmentFromModel($model,Attachment  $attachment)
    {
        $fileAdder = app(FileAdder::class);
        return $fileAdder->model($model)->attachment($attachment)->detachFromModel();
    }

  
}